---
title: Waste management
description: 
author: 
keywords: marp,marp-cli,slide
url: 
marp: true
image: 
---

# Waste management

---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# Problem statement

How to better handle unsorted (i.e. aggregate) construction waste.

---

# Collected stories

- _"People always just throw away useful material"_, said :construction_worker: on the :building_construction:
- _"Recycling is time-consuming and labor-intensive while boring"_, said :construction_worker: on the :building_construction:
- _"Depending on the type of construction and the phase, the type of waste varies greatly"_, said :construction_worker: on the :building_construction:
- ...

---

# Mindmap

::: fence

@startuml

@startmindmap

*[#Orange] Waste management
** Eliminate
**[#Orange] Reuse 
***[#Orange] Aggregate waste
**** by sorting
*****:Characteristic-
based sorting;
******_ Magnets to select metal waste
******_ Strainers to select particle size
******_ Centrifuges to select density
******_ ...
*****:Intelligence-
based sorting;
******_ Humans (incentive schemes)
******_ Machines (machine vision)
****[#Orange] without sorting
*** Seperated waste
**[#Orange] Dispose
***[#Orange] Aggregate waste
**** by sorting
*****[#FFFFFF] *same as above* 
****[#Orange] without sorting
*** Seperated waste

@endmindmap

@enduml

:::

<!--

REMOVED SECTION PERTAINING TO SEPARATION OF WASTE

**** Characteristic-based
***** Magnets to select metal waste
***** Strainers to select particle size
***** Centrifuges to select density
***** ...
**** Intelligence-based
***** Humans (through incentive/refund schemes)
***** Machine (through machine vision)

-->

---

# Concept

An outdoor device that is mounted separate from, but overlooking the contents of waste containers on construction sites. Using machine vision, the device is trained to detect the type of materials dumped into the container in real-time and correlate this information with the serial number (or any other unique identifier) on the waste container.

As a result, such aggregate waste can be traded for reuse and disposal in a more effective manner while not requiring any change in the way construction is done (a completely trasnparent solution).

---

# Open-source & hardware secondary research

![img](./assets/waste-small.jpg)
Na, Seunguk, et al. "Development of an artificial intelligence model to recognise construction waste by applying image data augmentation and transfer learning." Buildings 12.2 (2022): 175.
